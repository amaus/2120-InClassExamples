import java.util.ArrayList;

/**
* A generic Stack
*/
public class Stack<T> {
  private ArrayList<T> elements;
  
  /**
  * Construct an empty Stack
  */
  public Stack( ) {
    elements = new ArrayList<T>();
  }

  /**
  * Push an element onto the Stack
  * @param element the element to push
  */
  public void push(T element) {
    elements.add(element);
  }

  /**
  * Remove and return an element from the Stack
  * @return the element that was on the top of the Stack
  */
  public T pop( ) {
    return elements.remove(elements.size() - 1);
  }

  /**
  * {@inheritDoc}
  */  
  @Override
  public String toString( ) {
    String str = "";
    for(int i = elements.size()-1; i >= 0 ; i--) {
      str += String.format("%s\n",elements.get(i));
    }
    return str;
  }
}
