public class StackRunner {
  public static void main(String[] args) {
    Stack<Integer> stack = new Stack<Integer>();
    stack.push(3);
    stack.push(7);
    stack.push(5);
    stack.pop();
    stack.push(2);
    stack.push(8);

    System.out.print(stack);
  }
}
