public class QueueRunner {
  public static void main(String[] args) {
    Queue<String> queue = new Queue<String>();
    queue.enqueue("Bob");
    queue.enqueue("John");
    queue.enqueue("Tammy");
    queue.dequeue();
    queue.enqueue("Steve");
    queue.enqueue("Mark");
    queue.enqueue("Alice");

    System.out.print(queue);
  }
}
