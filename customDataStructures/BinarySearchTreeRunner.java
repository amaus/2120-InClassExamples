public class BinarySearchTreeRunner {
  public static void main(String[] args) {
    BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>(17);
    tree.insert(12);
    tree.insert(5);
    tree.insert(8);
    tree.insert(-6);
    tree.insert(0);
    tree.insert(7);
    tree.insert(29);

    System.out.println("Performing an In-Order Traversal");
    tree.inOrderTraversal();
    System.out.println();

    System.out.println("Printing out the Binary Search Tree");
    System.out.print(tree);
  }
}
