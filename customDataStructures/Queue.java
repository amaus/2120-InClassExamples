import java.util.LinkedList;
import java.util.Iterator;

/**
* A generic Queue
*/
public class Queue<T> {
  LinkedList<T> list;

  /**
  * Construct an empty Queue
  */
  public Queue( ) {
    list = new LinkedList<T>();
  }

  /**
  * Add an element to the end of the Queue
  * @param element the element to add
  */
  public void enqueue(T element) {
    list.addFirst(element);
  }

  /**
  * Remove and return the element from the beginning of the Queue
  * @return the element at the beginning of the queue
  */
  public T dequeue( ) {
    return list.removeLast();
  }

  /**
  * {@inheritDoc}
  */
  @Override
  public String toString( ) {
    String str = "";
    Iterator<T> descendingIt = list.descendingIterator();
    while(descendingIt.hasNext()) {
      str += String.format("%s - ", descendingIt.next());
    }
    return str;
  }
}
