public class GenericMethods {
  public static void main(String[] args) {
    String[] strings = {"A", "B", "C"};
    
    printArray(strings);

    Integer[] ints = {1, 2, 3};

    printArray(ints);

    System.out.println(maximum("A", "B"));
    System.out.println(maximum("apple", "alpha"));
  }

  public static <T> void printArray(T[] arr) {
    for(T element : arr) {
      System.out.print(element + " ");
    }
    System.out.println();
  }

  public static <T extends Comparable<T>> T maximum(T a, T b) {
    if(a.compareTo(b) < 0) {
      return b;
    } else {
      return a;
    }
  }
}
