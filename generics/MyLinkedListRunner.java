public class MyLinkedListRunner {
  public static void main(String[] args) {
    MyLinkedList<String> list = new MyLinkedList<String>();
    System.out.println(list);

    list.insert("Hello");
    list.insert("Hi");
    list.insert("Doggy");
    list.insert("World");
    list.insert("Bob");
    list.insert("Apple");
    list.insert("Sunny");
    list.insert("Clock");

    System.out.println(list);
  }
}
