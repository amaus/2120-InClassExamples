public class MyLinkedList<T> {
  private Node head;
  private Node tail;
  private int size;

  public MyLinkedList() {
    // create head and tail
    this.tail = new Node(null);
    this.head = new Node(null);
    this.head.setNext(tail);
    this.tail.setPrev(head);
    this.size = 0;
  }

  public void insert(T element) {
    // insert at the end
    Node node = new Node(element);
    Node tailsPrev = tail.getPrev();

    node.setNext(tail);
    node.setPrev(tailsPrev);
    tailsPrev.setNext(node);
    tail.setPrev(node);

    size++;
  }

  public String toString() {
    String str = "";
    if(this.size == 0) {
      str = "The Linked List is empty";
      return str;
    } else {
      Node current = this.head.getNext();
      do {
        str += String.format("[%s] - ", current.getElement());
        current = current.getNext();
      } while(current != tail);
    }
    return str;
  }

  private class Node {
    private Node prev;
    private Node next;
    private T element;

    Node( T element ) {
      this.prev = null;
      this.next = null;
      this.element = element;
    }

    void setPrev(Node prev) {
      this.prev = prev;
    }

    void setNext(Node next) {
      this.next = next;
    }

    Node getPrev() {
      return this.prev;
    }

    Node getNext() {
      return this.next;
    }

    T getElement() {
      return this.element;
    }
  }
}
