/**
* A simple implementation of a Binary Search Tree.
* <p>
* Supports the operations insert and inOrderTraversal.
*/
public class BinarySearchTree {
  private int data;
  private BinarySearchTree leftSubtree;
  private BinarySearchTree rightSubtree;
  private static int height; // used by toString() and buildTreeString()

  /**
  * Construct a Binary Search Tree
  * @param value the value held by this tree
  */
  public BinarySearchTree(int value) {
    this.data = value;
    this.leftSubtree = null;
    this.rightSubtree = null;
  }

  /**
  * @return the data held by this tree
  */
  public int getData() {
    return this.data;
  }

  /**
  * Insert a value into this BinarySearchTree
  * @param value the value to insert
  */
  public void insert(int value) {
    if(value < getData()) {
      // go left
      if(this.leftSubtree != null) {
        this.leftSubtree.insert(value);
      } else {
        this.leftSubtree = new BinarySearchTree(value);
      }
    } else {
      // go right
      if(this.rightSubtree != null) {
        this.rightSubtree.insert(value);
      } else {
        this.rightSubtree = new BinarySearchTree(value);
      }
    }
  }

  /**
  * Perform an In-Order Traversal of the Tree, printing out the values
  * in the tree.
  */
  public void inOrderTraversal() {
    if(this.leftSubtree != null) {
      this.leftSubtree.inOrderTraversal();
    }
    System.out.println(getData());
    if(this.rightSubtree != null) {
      this.rightSubtree.inOrderTraversal();
    }
  }

  /**
  * {@inheritDoc}
  */
  @Override
  public String toString() {
    height = 0;
    return "Root: Left Right\n"+buildTreeString();
  }

  /**
  * Build a String representation for this tree.
  * <p>
  * Each node in the tree gets a line formatted as: <p>
  * (17): (12) (29) <p>
  * meaning that (17) has a left child (12) and right (29)
  * @return the String representation of this tree
  */
  protected String buildTreeString() {
    // build up a String like:
    // (17): (12) (29)
    // meaning that (17) has a left child (12) and right (29)

    // First get the data in this tree
    String str = String.format("(%d): ", getData());

    // if the left subtree isn't null, append the value of the left
    // otherwise, append null
    if(this.leftSubtree != null) {
      str += String.format("(%d) ", this.leftSubtree.getData());
    } else {
      str += String.format("null ");
    }

    // if the right subtree isn't null, append the value of the right
    // otherwise, append null
    if(this.rightSubtree != null) {
      str += String.format("(%d)\n", this.rightSubtree.getData());
    } else {
      str += String.format("null\n");
    }

    // now recurse. Append a like string for the left subtree (if it
    // isn't null of course)
    if(this.leftSubtree != null) {
      str += this.leftSubtree.buildTreeString();
    }

    // likewise for the right subtree. recurse. Append a string for the
    // left subtree (if it isn't null of course)
    if(this.rightSubtree != null) {
      str += this.rightSubtree.buildTreeString();
    }

    return str;
  }
}
