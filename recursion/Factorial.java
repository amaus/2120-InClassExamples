public class Factorial {
  public static void main(String[] args) {
    System.out.println("4! is " + factorial(4));
  }

  /**
  * Return the factorial of n, n!.
  * @param n an integer, greater than 1
  * @return n!
  */
  public static int factorial(int n) {
    System.out.printf("%d! called\n",n);
    // Base Case: we know 1!. just return 1
    if (n == 1) {
      //System.out.printf("In Base Case, returning 1\n");
      return 1;
    }
    // The recursive step. n! is n*(n-1)!
    System.out.printf("Recursive Step, return %d * %d!\n",n, n-1);
    int recursiveStep = n * factorial(n-1);
    System.out.printf("Returning %d\n", recursiveStep);
    return recursiveStep;
  }
}
