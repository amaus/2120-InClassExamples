public class Fibonacci {
  public static void main(String[] args) {
    int index = 143;
    System.out.printf("Fibonacci(%d) = %d\n", index, fibonacci(index));
  }

  /**
  * Return the nth fibonacci number from the fibonacci sequence.
  * @param n the index of the number in the sequence
  * @return the value of that number in the sequence
  */
  public static int fibonacci(int n) {
    // Base Cases: The 0th and 1st fibonacci numbers are known
    if(n == 0) {
      return 0;
    }
    if(n == 1) {
      return 1;
    }
    // Recursive Step: return the previous two fibonacci numbers
    return fibonacci(n-1) + fibonacci(n-2);
  }
}
