import java.net.URL;
import java.util.Scanner;

public class URLReader {
  public static void main(String[] args) throws Exception {
    if(args.length != 1) {
      System.err.println("Usage: java URLReader <url>");
      System.exit(1);
    }
    String urlStr = args[0];

    URL url = new URL(urlStr);
    Scanner in = new Scanner(url.openStream());
    
    while(in.hasNext()) {
      System.out.println(in.nextLine());
    }
    in.close();
  }
}
