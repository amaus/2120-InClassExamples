import java.net.ServerSocket;
import java.net.Socket;

import java.io.IOException;
import java.io.PrintWriter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Server {
  public static void main(String[] args){
    if(args.length != 1) {
      System.err.println("Usage: java Server <port number>");
      System.exit(1);
    }

    int port = Integer.parseInt(args[0]);
    
    try (
      ServerSocket serverSocket = new ServerSocket(port);
      // wait for a client to attempt to connect
      Socket clientSocket = serverSocket.accept();
      Scanner in = new Scanner(clientSocket.getInputStream());
      PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
    ) {

      while(in.hasNext()) {
        String line = in.nextLine();
        System.out.println("Received input: " + line);
        System.out.println("Sending input back to client");
        out.println(line);
      }

    } catch (IOException e) {
      System.out.println("Could not connect to client");
      System.out.println(e.getMessage());
    }
  }
}
