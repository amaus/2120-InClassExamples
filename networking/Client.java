import java.net.Socket;
import java.net.UnknownHostException;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.util.Scanner;
 
public class Client {
  public static void main(String[] args) throws IOException {
    if (args.length != 2) {
      System.err.println("Usage: java EchoClient <host name> <port number>");
      System.exit(1);
    }
    String hostName = args[0];
    int port = Integer.parseInt(args[1]);

    try (
      Socket socket = new Socket(hostName, port);
      PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
      Scanner stdIn = new Scanner(System.in);
      Scanner serverIn = new Scanner(socket.getInputStream());
    ) {
      do {
        System.out.print("Enter text (q to quit): ");
        String text = stdIn.nextLine().trim();
        if(text.equals("q")) {
          break;
        }
        out.println(text);
        String serverResponse = serverIn.nextLine();
        System.out.println("Server Response: " + serverResponse);
      } while(true);
      
    } catch (UnknownHostException e) {
      System.out.printf("Could not connect to %s at port %d\n", hostName, port);
      System.out.println(e.getMessage());
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }
}
