import java.math.BigDecimal;
import java.util.Observable;

public class TipCalculatorModel extends Observable {

  private BigDecimal subtotal;
  private BigDecimal tipPercentage;
  private BigDecimal tip;
  private BigDecimal total;

  public TipCalculatorModel(final BigDecimal subtotal, 
                            final BigDecimal tipPercentage){
    this.subtotal = subtotal;
    this.tipPercentage = tipPercentage;
    refresh();
  }

  public BigDecimal getSubtotal() {
    return subtotal;
  }

  public BigDecimal getTipPercentage() {
    return tipPercentage;
  }

  public BigDecimal getTip() {
    return tip;
  }

  public BigDecimal getTotal() {
    return total;
  }

  public void setSubtotal(final BigDecimal subtotal){
    if (subtotal.compareTo(BigDecimal.ZERO) < 0){
      throw new IllegalArgumentException("Amount must be non-negative.");
    }
    this.subtotal = subtotal;
    refresh();
  }

  public void setTipPercentage(final BigDecimal tipPercentage){
    if (tipPercentage.compareTo(BigDecimal.ZERO) < 0){
      throw new IllegalArgumentException("Tip percentage must be non-negative.");
    }
    this.tipPercentage = tipPercentage;
    refresh();
  }

  private void refresh(){
    this.tip = this.subtotal.multiply(this.tipPercentage);
    this.total = this.subtotal.add(tip);
    this.setChanged();
    this.notifyObservers();
  }
}
