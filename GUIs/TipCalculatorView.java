import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Slider;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import java.util.Observer;
import java.util.Observable;
import java.math.BigDecimal;
import java.text.NumberFormat;

public class TipCalculatorView extends Application implements Observer{
  private GridPane gridPane;
  private TipCalculatorModel model;
  private TextField tipField;
  private TextField totalField; 
  private Label percentLabel;
  private static final NumberFormat PERCENT = NumberFormat.getPercentInstance();

  public static void main(String[] args) {
    Application.launch(args);
  }

  public void start(Stage stage) {
    stage.setTitle("Tip Calculator");

    this.gridPane = new GridPane();
    this.gridPane.setAlignment(Pos.CENTER);
    this.gridPane.setVgap(10);
    this.gridPane.setHgap(10);
    this.gridPane.setPadding(new Insets(25, 25, 25, 25));

    Scene scene = new Scene(this.gridPane, 250, 300);
    stage.setScene(scene);

    // build and add all components
    setupComponents();

    this.model = new TipCalculatorModel(new BigDecimal(0.0), new BigDecimal(0.2));
    this.model.addObserver(this);
    update(model, null);

    stage.show();

  }

  private void setupComponents() {
    Label label = new Label("Subtotal:");
    this.gridPane.add(label, 0, 0);

    TextField subtotalField = new TextField("$0.00");
    // add text field behavior
    subtotalField.textProperty().addListener(new ChangeListener<String>() {
        public void changed(ObservableValue<? extends String> observable, 
		String oldValue, 
		String newValue) {
          BigDecimal value = new BigDecimal(newValue);
          TipCalculatorView.this.model.setSubtotal(value);
        }
      });
    this.gridPane.add(subtotalField, 0, 1);

    this.percentLabel = new Label("Tip Percentage");
    this.gridPane.add(this.percentLabel, 0, 2);

    Slider slider = new Slider(0, 100, 20);
    // add slider behavior
    slider.valueProperty().addListener(new ChangeListener<Number>() {
        public void changed(ObservableValue<? extends Number> observable, 
		Number oldValue, 
		Number newValue) {
          BigDecimal value = new BigDecimal(newValue.doubleValue() / 100.0);
          TipCalculatorView.this.model.setTipPercentage(value);
        }
      });
    this.gridPane.add(slider, 0, 3);

    Label tipLabel = new Label("Tip");
    this.gridPane.add(tipLabel, 0, 4);
    
    this.tipField = new TextField("$0.00");
    this.tipField.setEditable(false);
    this.gridPane.add(tipField, 0, 5);
    
    Label totalLabel = new Label("Total");
    this.gridPane.add(totalLabel, 0, 6);

    this.totalField = new TextField("$0.00");
    this.totalField.setEditable(false);
    this.gridPane.add(totalField, 0, 7);
  }

  public void update(Observable o, Object arg) {
    this.tipField.setText(String.format("$%.2f", this.model.getTip()));
    this.totalField.setText(String.format("$%.2f", this.model.getTotal()));
    this.percentLabel.setText(String.format("Tip Percent %s:", 
        PERCENT.format(this.model.getTipPercentage())));
  }
  
}
