import java.util.List;

public class Producer {
  private int value = 0;

  public void produce(List<Integer> list) {
    synchronized (list) {
      list.add(0, value);
    }
    value++;
  }
}
