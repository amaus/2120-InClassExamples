import java.util.List;

public class Consumer {
  
  public void consume(List<Integer> list) {
    if(!list.isEmpty()) {
      synchronized (list) {
        System.out.println(list.remove(0));
      }
    }
  }
}
