import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.Arrays;
import java.util.ArrayList;

public class MultithreadedExample {
  private static ArrayList<Integer> sharedList;
  private static boolean keepRunning = true;

  public static void main(String[] args) {
    sharedList = new ArrayList<Integer>();

    ExecutorService executor = Executors.newCachedThreadPool();

    executor.execute(new Runnable() {
        public void run() {
          Producer producer = new Producer();
          while(keepRunning) {
            producer.produce(sharedList);
          }
        }
      });

    executor.execute(new Runnable() {
        public void run() {
          Consumer consumer = new Consumer();
          while(keepRunning) {
            consumer.consume(sharedList);
          }
        }
      });

    executor.shutdown();

    // sleep this main thread for 1000 ms, letting the children play
    try {
      Thread.sleep(100);
    } catch(InterruptedException e) {
      Thread.currentThread().interrupt();
    }

    // stop both children
    keepRunning = false;

    // print out the sharedList
    int counter = 0;
    for(Integer val : sharedList) {
      System.out.printf("%6s", val);
      if(counter != 0 && counter % 15 == 0) {
        System.out.println();
      }
      counter++;
    }
  }
}
