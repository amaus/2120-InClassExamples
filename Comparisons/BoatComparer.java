import java.util.Collections;
import java.util.ArrayList;
import java.util.Comparator;

public class BoatComparer{
  public static void main(String[] args) {
    ArrayList<Boat> boats = new ArrayList<Boat>();
    boats.add(new Boat("Mary", 55.0, 12.0));
    boats.add(new Boat("Express", 70.5, 15.0));
    boats.add(new Boat("Enterprise", 450, 40.0));
    boats.add(new Boat("Lil Tugger", 5, 14.0));
    
    System.out.println("Sorting on Tonnage");
    Collections.sort(boats);
    printBoats(boats);
    System.out.println();

    System.out.println("Now sorting on Speed");
    //Collections.sort(boats, new BoatSpeedComparator());
    //printBoats(boats);

    // Using an anonymous class to implement the comparator
    Collections.sort(boats, 
      new Comparator<Boat>() {
        public int compare(Boat o1, Boat o2) {
          double diff = o1.getSpeed() - o2.getSpeed();
          if(diff < 0) {
            return -1;
          } else if (diff > 0) {
            return 1;
          }
          return 0;
        }
      }
    ); // end of sort method call
    printBoats(boats);
  }

  public static void printBoats(ArrayList<Boat> boats) {
    for(Boat boat : boats) {
      System.out.printf("%s: %.2f tons  %.2f knots\n",
          boat.getName(), boat.getTonnage(), boat.getSpeed());
    }
  }
}
