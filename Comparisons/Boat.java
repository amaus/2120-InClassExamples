public class Boat implements Comparable<Boat> {
  private String name;
  private double tonnage;
  private double speed; // in knots
  
  public Boat(String name, double tonnage, double speed) {
    this.name = name;
    this.tonnage = tonnage;
    this.speed = speed;
  }

  public String getName() {
    return this.name;
  }

  public double getTonnage() {
    return this.tonnage;
  }

  public double getSpeed() {
    return this.speed;
  }

  @Override
  public int compareTo(Boat other) {
    double diff = this.getTonnage() - other.getTonnage();
    if(diff < 0) {
      return -1;
    } else if (diff > 0) {
      return 1;
    } else {
      return 0;
    }
  }
}
