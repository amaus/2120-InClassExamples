import java.util.Comparator;
public class BoatSpeedComparator implements Comparator<Boat> {

  @Override
  public int compare(Boat o1, Boat o2) {
    double diff = o1.getSpeed() - o2.getSpeed();
    if(diff < 0) {
      return -1;
    } else if (diff > 0) {
      return 1;
    } else {
      return 0;
    }
  }

}
